import { Component } from "react";
import icon from "../../../assets/images/likeicon.png"

class OutputContent extends Component {
    render() {
        let { outputMessageProps, likeDisplayProps } = this.props;

        return (
            <>
                <div className="mt-3">
                {outputMessageProps.map((element, index) => {
                    return <p key={index}>{element}</p>
                })}
                </div>
                <div className="mt-3">
                    {likeDisplayProps ? <img src={icon} alt="Devcamp120" width="50" /> : <></>}
                </div>
            </>

        )
    }
}

export default OutputContent;