import { Component } from "react";

import InputContent from "./input/inputContent";
import OutputContent from "./output/outputContent";

    class Content extends Component {
        constructor(props) {
            super(props);

            this.state = {
                inputMessage: "",
                outputMessage: [],
                likeDisplay: false
            }
        }

        inputMessageChangeHandler = (value) => {
            this.setState({
                inputMessage: value
            })
        }

        outputMessageChangeHandler = () => {
            if(this.state.inputMessage) {
                this.setState({
                    outputMessage : [...this.state.outputMessage, this.state.inputMessage],
                    likeDisplay: true
                })
            }
            
        }

        render() {
            return (
                <>
                    <InputContent inputMessageProps={this.state.inputMessage} inputMessageChangeHandlerProps={this.inputMessageChangeHandler} outputMessageChangeHandlerProps={this.outputMessageChangeHandler} />
                    <OutputContent outputMessageProps={this.state.outputMessage} likeDisplayProps={this.state.likeDisplay} />
                </>
            )
        }
    }

export default Content;