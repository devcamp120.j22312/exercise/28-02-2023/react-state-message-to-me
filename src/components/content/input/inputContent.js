import { Component } from "react";

 
class InputContent extends Component {
    inputChangeHandler = (event) => {
        let value = event.target.value;
    
        this.props.inputMessageChangeHandlerProps(value);
    }
    
     onClickHandler = () => {
        console.log("Đã bấm nút gửi thông điệp");
    
        this.props.outputMessageChangeHandlerProps();
    }

    render() {
        return (
            <>
                <div className="mt-3">
                    <label>Message cho bạn 12 tháng tới</label>
                </div>
                <div className="mt-3">
                    <input className="form-control" value={this.props.inputMessageProps} onChange={this.inputChangeHandler}></input>
                </div>
                <div className="mt-3">
                    <button onClick={this.onClickHandler} className="btn btn-primary">Gửi thông điệp</button>
                </div>
            </>

        )
    }
}

export default InputContent;