import { Component } from "react";
import image from "../../assets/images/developer.png"

class Header extends Component {
    render() {
        return (
            <>
                <div className="row">
                    <div className="col-12">
                        <h1>Chào mừng đến với Devcamp 120</h1>
                    </div>
                </div>
                <div className="mt-3">
                    <img src={image} alt="Devcamp120" width="500" />
                </div>
            </>
        )
    }
}

export default Header;